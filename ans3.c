#include <stdio.h>

int main(int argc, char *argv[])
{
	int rows1, cols1, rows2, cols2;
	int operation;
	int matrix1[10][10];
	int matrix2[10][10];
	int result[10][10];

	puts("Select the operation you need to do for 2 matrices:");
	puts("Note: maximum dimension size this program currently supporting is 10 x 10.");
	printf("1 = add\n2 = multiply\n?> ");
	scanf("%d", &operation);

	if (!(operation == 1 || operation == 2))
	{
		puts("Please enter the right operation number");
		return 0;
	}

	printf("Enter the rows and cols for matrix 1: ");
	scanf("%d %d", &rows1, &cols1);


	for (int i = 0; i < rows1; i++)
	{
		for (int j = 0; j < cols1; j++)
		{
			scanf("%d", &matrix1[i][j]);
		}
	}

	printf("\nEnter the rows and cols for matrix 2: ");
	scanf("%d %d", &rows2, &cols2);

	for (int i = 0; i < rows2; i++)
	{
		for (int j = 0; j < cols2; j++)
		{
			scanf("%d", &matrix2[i][j]);
		}
	}

	for (int i = 0; i < rows1; i++)
		for (int j = 0; j < cols2; j++)
			result[i][j] = 0;

	if (cols1 == rows2 && operation == 2)
	{
		for (int r = 0; r < rows1; r++)
		{
			for (int c = 0; c < cols2; c++)
			{
				result[r][c] = 0;
				for (int k = 0; k <= (cols2 == 1 ? cols1 - 1 : cols2); k++)
				{
					result[r][c] += matrix1[r][k] * matrix2[k][c];
					if (cols1 == rows2 && cols1 == 1)
						break;
				}
			}
		}
	}
	else if(cols1 == cols2 && rows1 == rows2)
	{
		for (int r = 0; r < rows1; r++)
		{
			for (int c = 0; c < cols2; c++)
			{
				result[r][c] = matrix1[r][c] + matrix2[r][c];
			}
		}
	}
	else
	{
		puts("Matrix dimensions error!");
		return 0;
	}

	puts("\nResult matrix is:");

	for (int r = 0; r < rows1; r++)
	{
		for (int c = 0; c < cols2; c++)
		{
			printf("%*d ", 2, result[r][c]);
		}
		puts("");
	}

	return 0;
}
