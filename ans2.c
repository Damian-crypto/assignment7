#include <stdio.h>
#include <string.h>

int main()
{
	char str[1024];
	char c;
	int frequency = 0;
	printf("Enter a string to find frequency of a character: ");
	fgets(str, sizeof str, stdin);
	printf("Enter the character you want to find the frequency: ");
	scanf("%c", &c);

	for (int i = 0; i < strlen(str); i++)
		frequency += str[i] == c ? 1 : 0;

	printf("Frequency of the '%c' in the string is: %d\n", c, frequency);

	return 0;
}

