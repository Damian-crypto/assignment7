#include <stdio.h>
#include <string.h>

int main()
{
	char arr[1024];
	printf("Enter a string to reverse: ");
	fgets(arr, sizeof arr, stdin);
	
	for (int i = strlen(arr); i > -1; i--)
		printf("%c", arr[i]);
	puts("");

	return 0;
}

